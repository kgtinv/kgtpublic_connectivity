﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;
using Kreslik.Integrator.QuickItchN;

namespace ITCHConnectivityPrototype
{
    class Program
    {
        static void Main(string[] args)
        {

            CommunicationTest02();

            ////MemoryStream ms = new MemoryStream(System.Text.Encoding.ASCII.GetBytes("AHHSSLAGTHSFS"));

            ////StreamWrapper sw = new StreamWrapper(ms, 100);

            ////for (int i = 0; i < 100; i++)
            ////{
            ////    Console.WriteLine(sw.ReadNextToken((byte) 'S'));
            ////}



            //string username = "kgt2";
            //string password = "hotspot";

            //LoginRequestItchMessage msg0 = new LoginRequestItchMessage(username, password, true);
            //var str = msg0.RawMessage;

            //ILogger logger = LogFactory.Instance.GetLogger("ItchTest");


            ////StringBuilder messageBuilder = new StringBuilder();
            ////messageBuilder.Append(/*0x4C*/'\u004C');
            ////messageBuilder.AppendFormat("{0,-40}{1,-40}{2}{3,9}{4}", username, password, /*0x46*/'\u0046', 0, '\u000A');

            ////var str = messageBuilder.ToString();

            ////kgt2, hotspot
            //ItchClient ic = new ItchClient("209.191.250.157", 9013, logger);
            //ic.NewItchMessage += message => logger.Log(LogLevel.Info, "Client code received: {0}", message);
            //ic.Initialize();

            //Thread.Sleep(TimeSpan.FromSeconds(2));
            //Console.ReadKey();

            //StringBuilder messageBuilder = new StringBuilder();
            ////messageBuilder.Append(/*0x4C*/'\u004C');
            ////messageBuilder.AppendFormat("{0,-40}{1,-40}{2}{3,9}{4}", username, password, '\u0046'/*'\u0054'*/, 0, '\u000A');

            ////ic.SendMessage(messageBuilder.ToString());

            //LoginRequestItchMessage msg = new LoginRequestItchMessage(username, password, false);
            //ic.SendMessage(msg);

            //Thread.Sleep(TimeSpan.FromSeconds(2));

            //Console.WriteLine("Press a key to exit...");
            //Console.ReadKey();

            //messageBuilder = new StringBuilder();
            //messageBuilder.Append(/*0x4C*/'\u004F');
            //messageBuilder.Append('\u000A');
            //ic.SendMessageDeprecated(messageBuilder.ToString());
            //Thread.Sleep(TimeSpan.FromSeconds(2));

            //ic.Stop();

            //Thread.Sleep(TimeSpan.FromSeconds(2));
            //Console.ReadKey();
            //Thread.Sleep(TimeSpan.FromSeconds(2));
        }


        private static void CommunicationTest01()
        {
            string username = "kgt2";
            string password = "hotspot";
            string hostname = "209.191.250.157";
            int port = 9013;

            ILogger logger = LogFactory.Instance.GetLogger("ItchTest");

            ItchClient itchClient = new ItchClient(hostname, port, logger, TimeSpan.FromSeconds(14));
            itchClient.NewIncomingItchMessage += message => logger.Log(LogLevel.Info, "Client receiving: {0}", message);
            itchClient.Connected += () => logger.Log(LogLevel.Info, "Session is now connected");
            itchClient.Disconnected += () => logger.Log(LogLevel.Warn, "Session is now closed");
            itchClient.Connect(username, password);

            Console.ReadKey();

            itchClient.SendMessage(new InstrumentDirectoryRequestItchMessage());
            //itchClient.SendMessage(new MarketSnapshotRequestItchMessage());
            //itchClient.SendMessage(new MarketDataSubscribeItchMessage());
            itchClient.SendMessage(new TickerSubscribeItchMessage());

            Console.ReadKey();

            itchClient.Close();

            Console.ReadKey();
        }


        private static void CommunicationTest02()
        {
            string username = "kgt2";
            string passwordWrong = "WrongPassword";
            string password = "hotspot";
            string hostname = "209.191.250.157";
            string hostnameWrong = "210.191.250.157";
            int port = 9013;

            ILogger logger = LogFactory.Instance.GetLogger("ItchTest");

            //HotspotMarketSession cracker = new HotspotMarketSession();

            ItchSession itchSession = new ItchSession();
            //itchSession.Configure(/*hostname, port, username, passwordWrong, TimeSpan.FromSeconds(14),*/ logger);
            itchSession.Configure(hostname, port, username, password, TimeSpan.FromSeconds(14), logger);
            //itchSession.NewIncomingItchMessage += message =>
            //    {
            //        logger.Log(LogLevel.Info, "Client receiving: {0}", message);
            //        SequencedDataItchMessage sqd = message as SequencedDataItchMessage;
            //        if (sqd != null)
            //        {
            //            logger.Log(LogLevel.Warn, "Integrator receive delay: {0}", sqd.IntegratorReceivedTimeUtc - sqd.CounterpartySentTime);
            //        }

            //        //if (!cracker.Crack(message))
            //        //{
            //        //    logger.Log(LogLevel.Info, "No handler found for message");
            //        //}
            //    };
            itchSession.LoggedIn += () => logger.Log(LogLevel.Info, "Session is now logged in");
            itchSession.LoggedOut += () => logger.Log(LogLevel.Warn, "Session is now logged out");

            itchSession.Start();

            //Console.ReadKey();

            //itchSession.Configure(hostname, port, username, passwordWrong, TimeSpan.FromSeconds(14), logger);
            //itchSession.Restart();

            Console.WriteLine("Pres a key to subscribe to all pairs...");
            Console.ReadKey();

            itchSession.SendMessage(new InstrumentDirectoryRequestItchMessage());
            itchSession.SendMessage(new MarketSnapshotRequestItchMessage());
            itchSession.SendMessage(new MarketDataSubscribeItchMessage());
            itchSession.SendMessage(new TickerSubscribeItchMessage());

            Console.WriteLine("Pres a key to stop the session...");
            Console.ReadKey();

            itchSession.Stop();

            Console.WriteLine("Pres a key to eixt...");
            Console.ReadKey();
        }


        
    }

    

    

    



    

    

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.QuickItchN
{
    public interface IItchClient
    {
        void Connect(string login, string password);
        event Action<IncomingItchMessage> NewIncomingItchMessage;
        event Action Disconnected;
        event Action Connected;
        void SendMessage(OutgoingItchMessage message);
        void Close();
    }
}

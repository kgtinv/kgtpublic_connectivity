﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.QuickItchN
{
    public class ItchOrder
    {
        public ItchOrder(DealDirection side, Symbol symbol, string counterpartyOrderId, decimal price, decimal size,
                         decimal? minqty, decimal? lotsize)
        {
            this.Side = side;
            this.Symbol = symbol;
            this.CounterpartyOrderId = counterpartyOrderId;
            this.Price = price;
            this.Size = size;
            this.Minqty = minqty;
            this.Lotsize = lotsize;
        }

        public DealDirection Side { get; private set; }
        public Symbol Symbol { get; private set; }
        public string CounterpartyOrderId { get; private set; }
        public decimal Price { get; private set; }
        public decimal Size { get; private set; }
        public decimal? Minqty { get; private set; }
        public decimal? Lotsize { get; private set; }

        public override string ToString()
        {
            return string.Format("ItchOrder [{0}] - Attempt to {1} {2} of {3} (minqty: {4}, lotsize: {5}) at {6}.",
                                 CounterpartyOrderId, Side, Size, Symbol, Minqty.HasValue ? Minqty.ToString() : "NOT SPECIFIED",
                                 Lotsize.HasValue ? Lotsize.ToString() : "NOT SPECIFIED", Price);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.QuickItchN
{
    public abstract class MessageCrackerBase<TDerivedType> where TDerivedType : MessageCrackerBase<TDerivedType>
    {
        private Dictionary<Type, Action<TDerivedType, IncomingItchMessage>> _handlerMethods =
            new Dictionary<Type, Action<TDerivedType, IncomingItchMessage>>();

        public MessageCrackerBase()
        {
            initialize(this);
        }

        private void initialize(Object messageHandler)
        {
            Type handlerType = messageHandler.GetType();

            MethodInfo[] methods = handlerType.GetMethods();
            foreach (MethodInfo m in methods)
            {
                if (IsHandlerMethod(m))
                {
                    _handlerMethods[m.GetParameters()[0].ParameterType] = MagicMethod<TDerivedType>(m);
                }
            }
        }

        //Precompiling the delegate is far more efficient (also no GC will be involved during calling)
        // and it's type safe and exceptions are being thrown directly
        private Action<T,IncomingItchMessage> MagicMethod<T>(MethodInfo method)
        {
            var parameter1 = method.GetParameters().First();
            var instance = Expression.Parameter(typeof(T), "instance");
            var argument1 = Expression.Parameter(typeof(IncomingItchMessage), "messageArgument");
            var methodCall = Expression.Call(
                instance,
                method,
                Expression.Convert(argument1, parameter1.ParameterType)
                );
            return Expression.Lambda<Action<T, IncomingItchMessage>>(
                Expression.Convert(methodCall, typeof(void)),
                instance, argument1).Compile();
        }

        static public bool IsHandlerMethod(MethodInfo m)
        {
            return (m.IsPublic == true
                && m.Name.Equals("OnMessage")
                && m.GetParameters().Length == 1
                //&& m.GetParameters()[0].ParameterType.IsSubclassOf(typeof(QuickFix.Message))
                && typeof(IncomingItchMessage).IsAssignableFrom(m.GetParameters()[0].ParameterType)
                && m.ReturnType == typeof(void));
        }


        /// <summary>
        /// Process ("crack") an ITCH message and call the registered handlers for that type, if any
        /// </summary>
        /// <param name="message"></param>
        public bool Crack(IncomingItchMessage message)
        {
            Type messageType = message.GetType();
            Action<TDerivedType, IncomingItchMessage> handler = null;

            if (_handlerMethods.TryGetValue(messageType, out handler))
            {
                handler((TDerivedType)this, message);

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

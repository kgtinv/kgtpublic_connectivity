﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.QuickItchN
{
    public class ModifyOrderItchMessage : SequencedDataItchMessage
    {
        public ModifyOrderItchMessage(string rawMessage, Symbol symbol, string counterpartyOrderId, decimal size, decimal? minqty, decimal? lotsize)
            : base(rawMessage, ItchSequencedDataMessageSubtype.ModifyOrder)
        {
            this.Symbol = symbol;
            this.CounterpartyOrderId = counterpartyOrderId;
            this.Size = size;
            this.Minqty = minqty;
            this.Lotsize = lotsize;
        }

        public Symbol Symbol { get; private set; }
        public string CounterpartyOrderId { get; private set; }
        public decimal Size { get; private set; }
        public decimal? Minqty { get; private set; }
        public decimal? Lotsize { get; private set; }

        public override string ToString()
        {
            return string.Format("ModifyOrder [{0}] - New Size: {1} (of {2}) (minqty: {3}, lotsize: {4}). {5}",
                                 CounterpartyOrderId, Size, Symbol, Minqty.HasValue ? Minqty.ToString() : "NOT SPECIFIED",
                                 Lotsize.HasValue ? Lotsize.ToString() : "NOT SPECIFIED", base.ToString());
        }

        public static bool TryParseModifyOrderMessage(string messageString, ref int position, ILogger logger, out SequencedDataItchMessage sequencedItchMessage)
        {
            sequencedItchMessage = null;

            if (messageString.Length - position != 71 && messageString.Length - position != 39)
            {
                logger.Log(LogLevel.Error, "ModifyOrder message size [{0}] is unexpected (Expected 71 or 39 in data packet part, was {1}): {2}", messageString.Length, messageString.Length - position, messageString);
                return false;
            }

            Symbol symbol;
            if (!Symbol.TryParse(messageString.Substring(position, 7), out symbol))
            {
                logger.Log(LogLevel.Error, "Experienced unknown currency pair: {0} in ModifyOrder message: {1}",
                                 messageString.Substring(position, 7), messageString);
                return false;
            }
            position += 7;

            string counterpartyOrderId = messageString.Substring(position, 15);
            position += 15;

            string amountString = messageString.Substring(position, 16);
            position += 16;

            decimal size;
            if (!decimal.TryParse(amountString, NumberStyles.Any, CultureInfo.InvariantCulture, out size))
            {
                logger.Log(LogLevel.Error, "Experienced unparsable size: {0} in ModifyOrder message: {1}",
                                 amountString, messageString);
                return false;
            }

            //MinQty and LotSize not present
            if (messageString.Length - position == 0)
            {
                logger.Log(LogLevel.Warn, "Minqty and LotSize not present in ModifyOrder message: {0}", messageString);
                sequencedItchMessage = new ModifyOrderItchMessage(messageString, symbol, counterpartyOrderId, size, null, null);
                return true;
            }

            string minqtyString = messageString.Substring(position, 16);
            position += 16;

            decimal minQty;
            if (!decimal.TryParse(minqtyString, NumberStyles.Any, CultureInfo.InvariantCulture, out minQty))
            {
                logger.Log(LogLevel.Error, "Experienced unparsable MinQty: {0} in ModifyOrder message: {1}",
                                 minqtyString, messageString);
                return false;
            }

            string lotsizeString = messageString.Substring(position, 16);
            position += 16;

            decimal lotSize;
            if (!decimal.TryParse(lotsizeString, NumberStyles.Any, CultureInfo.InvariantCulture, out lotSize))
            {
                logger.Log(LogLevel.Error, "Experienced unparsable LotSize: {0} in ModifyOrder message: {1}",
                                 lotsizeString, messageString);
                return false;
            }

            sequencedItchMessage = new ModifyOrderItchMessage(messageString, symbol, counterpartyOrderId, size, minQty, lotSize);
            return true;
        }
    }
}

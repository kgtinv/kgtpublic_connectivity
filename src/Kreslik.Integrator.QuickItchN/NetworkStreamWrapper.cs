﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.QuickItchN
{
    public class NetworkStreamWrapper : INetworkStreamWrapper
    {
        private string _hostName;
        private int _port;
        private Stream _stream;
        private IStreamWrapper _streamWrapper;
        private readonly int _maxSingleMessageSize;

        public NetworkStreamWrapper(string hostName, int port, int maxSingleMessageSize)
        {
            this._hostName = hostName;
            this._port = port;
            this._maxSingleMessageSize = maxSingleMessageSize;
        }

        public void Initialize()
        {
            this._stream = CreateClientStream(this._hostName, this._port);
            this._streamWrapper = new StreamWrapper(_stream, this._maxSingleMessageSize);
        }

        private static Stream CreateClientStream(string hostName, int port)
        {
            IPAddress[] addrs = Dns.GetHostAddresses(hostName);
            IPEndPoint endpoint = new IPEndPoint(addrs[0], port);

            var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.NoDelay = true; //if false or no setting then there would be bufering and delays
            //socket.ReceiveTimeout = 1000; // this would cause IOExceptions if no data came within timeout
            try
            {
                socket.Connect(endpoint);
            }
            catch (Exception)
            {
                //fast release resources - do not wait and relay on GC (what if we have zero GC)
                socket.Dispose();
                throw;
            }
            
            Stream stream = new NetworkStream(socket, ownsSocket: true);

            return stream;
        }

        public void SendString(string message)
        {
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(message);
            _stream.Write(bytes, 0, bytes.Length);
        }

        public void Stop()
        {
            if (_stream != null && _stream.CanRead)
            {
                _stream.Close();
            }
        }

        public string ReadNextToken(byte tokenSeparator)
        {
            return this._streamWrapper.ReadNextToken(tokenSeparator);
        }

        public override string ToString()
        {
            return string.Format("NetworkStreamWrapper - hostname: {0}, port: {1}", _hostName, _port);
        }
    }
}

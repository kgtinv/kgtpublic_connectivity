﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.QuickItchN
{
    public interface INetworkStreamWrapper : IStreamWrapper
    {
        void SendString(string message);
        void Initialize();
        void Stop();
    }
}

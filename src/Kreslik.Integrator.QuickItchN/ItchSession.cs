﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.QuickItchN
{

    public class ItchSession : IItchSession
    {
        private string _hostName;
        private int _port;
        private string _login;
        private string _password;
        private TimeSpan _heartBeatInterval;
        private ILogger _logger;

        public void Configure(string hostName, int port, string login, string password, TimeSpan heartBeatInterval, ILogger logger)
        {
            this._hostName = hostName;
            this._port = port;
            this._login = login;
            this._password = password;
            this._heartBeatInterval = heartBeatInterval;
            this._logger = logger;

            if (ItchSessionState == ItchSessionState.Unconfigured)
            {
                this.ItchSessionState = ItchSessionState.Created;
            }
        }

        public void Configure(QuickItchNSettings.SessionSettings sessionSettings, ILogger logger)
        {
            this.Configure(sessionSettings.Host, sessionSettings.Port, sessionSettings.Username,
                sessionSettings.Password, sessionSettings.HeartBeatInterval, logger);
        }

        private ItchSessionState _itchSessionState;

        public ItchSessionState ItchSessionState
        {
            get { return this._itchSessionState; }
            private set
            {
                if (this._itchSessionState != value)
                {
                    this._itchSessionState = value;
                    if (this.StateChanging != null)
                        this.StateChanging(value);
                }
            }
        }

        public event Action<ItchSessionState> StateChanging;

        public void Configure(ILogger logger, HotspotCounterparty counterparty)
        {
            if (counterparty == HotspotCounterparty.HTA)
            {
                this.Configure(QuickItchNSettings.QuickItchNBehavior.HtaSessionSettings, logger);
            }
            else if (counterparty == HotspotCounterparty.HTF)
            {
                this.Configure(QuickItchNSettings.QuickItchNBehavior.HtfSessionSettings, logger);
            }
            else if (counterparty == HotspotCounterparty.HT3)
            {
                this.Configure(QuickItchNSettings.QuickItchNBehavior.Ht3SessionSettings, logger);
            }
            else if (counterparty == HotspotCounterparty.H4T)
            {
                this.Configure(QuickItchNSettings.QuickItchNBehavior.H4tSessionSettings, logger);
            }
            else if (counterparty == HotspotCounterparty.H4M)
            {
                this.Configure(QuickItchNSettings.QuickItchNBehavior.H4mSessionSettings, logger);
            }
            else
            {
                throw new ArgumentException("Invalid Hotspot counterparty specified", "counterparty");
            }
        }

        private IItchClient _itchClient;
        private CancellationTokenSource _reconnectCancellationTokenSource;

        public bool Inactivated
        {
            get
            {
                return _reconnectCancellationTokenSource == null ||
                       _reconnectCancellationTokenSource.IsCancellationRequested;
            }
        }

        public bool Start()
        {
            if (ItchSessionState == ItchSessionState.Unconfigured)
            {
                this._logger.Log(LogLevel.Error,
                                 "Cannot start itch client for unconfigured session. Run Configure() first");
                return false;
            }

            if (!(ItchSessionState == ItchSessionState.Created || ItchSessionState == ItchSessionState.Stopped))
            {
                this._logger.Log(LogLevel.Error, "Cannot start ITCH client that is currently running");
                return false;
            }

            _itchClient = new ItchClient(this._hostName, this._port, this._logger, this._heartBeatInterval);
            _itchClient.Connected += OnClientConnected;
            _itchClient.Disconnected += OnClientDisconnected;
            _itchClient.NewIncomingItchMessage += _newIncomingItchMessageInternal;

            if (_reconnectCancellationTokenSource == null ||
                _reconnectCancellationTokenSource.IsCancellationRequested)
            {
                _reconnectCancellationTokenSource = new CancellationTokenSource();
            }
            ItchSessionState = ItchSessionState.Starting;

            TaskEx.StartNew(() => _itchClient.Connect(this._login, this._password));

            return true;
        }

        public bool Stop()
        {
            _reconnectIntervalIdx = 0;
            if (_reconnectCancellationTokenSource != null)
            {
                _reconnectCancellationTokenSource.Cancel();
            }

            if (_itchClient == null || ItchSessionState != ItchSessionState.Running)
            {
                this._logger.Log(LogLevel.Error, "Cannot stop ITCH client that is not currently running");
                return false;
            }

            ItchSessionState = ItchSessionState.Stopping;
            _itchClient.Close();
            return true;
        }

        private bool _immediateRestartNeeded = false;
        public void Restart()
        {
            _immediateRestartNeeded = true;
            this.Stop();
        }

        //this is directly subscribed to ItchClient.Connected evvent - so exceptions are handeled there
        public event Action LoggedIn;

        //this is directly subscribed to ItchClient.Disconnected evvent - so exceptions are handeled there
        public event Action LoggedOut;

        public event Action<IncomingItchMessage> NewIncomingItchMessage
        {
            add
            {
                if (_itchClient != null)
                {
                    _itchClient.NewIncomingItchMessage += value;
                }
                _newIncomingItchMessageInternal += value;
            }
            remove
            {
                if (_itchClient != null)
                {
                    _itchClient.NewIncomingItchMessage -= value;
                }
                _newIncomingItchMessageInternal -= value;
            }
        }
        private Action<IncomingItchMessage> _newIncomingItchMessageInternal;

        public void SendMessage(OutgoingItchMessage message)
        {
            if (_itchClient != null && ItchSessionState == ItchSessionState.Running)
            {
                _itchClient.SendMessage(message);
            }
            else
            {
                this._logger.Log(LogLevel.Error, "Cannot send message through not connected or disconnected client: {0}", message);
            }
        }

        private void OnClientConnected()
        {
            this.ItchSessionState = ItchSessionState.Running;
            _reconnectIntervalIdx = 0;

            if (LoggedIn != null)
            {
                LoggedIn();
            }
        }

        private int _reconnectIntervalIdx = 0;
        private TimeSpan[] _reconnectIntervals = new TimeSpan[] { TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(10), TimeSpan.FromSeconds(30), TimeSpan.FromMinutes(1), TimeSpan.FromMinutes(5), TimeSpan.FromMinutes(10) };

        private void OnClientDisconnected()
        {
            this.ItchSessionState = ItchSessionState.Stopped;

            if (_itchClient != null)
            {
                _itchClient.Connected -= OnClientConnected;
                _itchClient.Disconnected -= OnClientDisconnected;
                _itchClient.NewIncomingItchMessage -= _newIncomingItchMessageInternal;
            }

            if (LoggedOut != null)
            {
                LoggedOut();
            }

            _reconnectCancellationTokenSource.Token.WaitHandle.WaitOne(_reconnectIntervals[_reconnectIntervalIdx]);
            if (!_reconnectCancellationTokenSource.IsCancellationRequested || _immediateRestartNeeded)
            {
                _immediateRestartNeeded = false;
                _reconnectIntervalIdx = Math.Min(_reconnectIntervalIdx + 1, _reconnectIntervals.Length - 1);
                this.Start();
            }
            else
            {
                this._logger.Log(LogLevel.Info, "Stop was requested, not retrying to reconnect");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.QuickItchN
{
    public class EndOfSessionItchMessage : SequencedDataItchMessage
    {
        public EndOfSessionItchMessage(string rawMessage)
            : base(rawMessage, ItchSequencedDataMessageSubtype.EndOfSession)
        { }

        public override bool IsAdminMessage
        {
            get { return true; }
        }
    }
}

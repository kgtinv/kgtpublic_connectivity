﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.QuickItchN
{
    public class NewOrderItchMessage : SequencedDataItchMessage
    {
        public NewOrderItchMessage(string rawMessage, DealDirection side, Symbol symbol, string counterpartyOrderId,
                                   decimal price, decimal size, decimal? minqty, decimal? lotsize)
            : base(rawMessage, ItchSequencedDataMessageSubtype.NewOrder)
        {
            this.ItchOrder = new ItchOrder(side, symbol, counterpartyOrderId, price, size, minqty, lotsize);
        }

        public ItchOrder ItchOrder { get; private set; }

        public override string ToString()
        {
            return string.Format("NewOrder: {0}. {1}", ItchOrder, base.ToString());
        }

        public static bool TryParseNewOrderMessage(string messageString, ref int position, ILogger logger, out SequencedDataItchMessage sequencedItchMessage)
        {
            sequencedItchMessage = null;

            if (messageString.Length - position != 82 && messageString.Length - position != 50)
            {
                logger.Log(LogLevel.Error, "NewOrder message size [{0}] is unexpected (Expected 82 or 50 in data packet part, was {1}): {2}", messageString.Length, messageString.Length - position, messageString);
                return false;
            }

            DealDirection side;
            switch (messageString[position])
            {
                case 'B':
                    side = DealDirection.Buy;
                    break;
                case 'S':
                    side = DealDirection.Sell;
                    break;
                default:
                    logger.Log(LogLevel.Error,
                                     "Experienced unexpected side indicator character: {0} in NewOrder message: {1}",
                                     messageString[position], messageString);
                    return false;
            }
            position++;

            Symbol symbol;
            if (!Symbol.TryParse(messageString.Substring(position, 7), out symbol))
            {
                logger.Log(LogLevel.Error, "Experienced unknown currency pair: {0} in NewOrder message: {1}",
                                 messageString.Substring(position, 7), messageString);
                return false;
            }
            position += 7;

            string counterpartyOrderId = messageString.Substring(position, 15);
            position += 15;

            string priceString = messageString.Substring(position, 10);
            position += 10;

            decimal price;
            if (!decimal.TryParse(priceString, NumberStyles.Any, CultureInfo.InvariantCulture, out price))
            {
                logger.Log(LogLevel.Error, "Experienced unparsable price: {0} in NewOrder message: {1}",
                                 priceString, messageString);
                return false;
            }

            string amountString = messageString.Substring(position, 16);
            position += 16;

            decimal size;
            if (!decimal.TryParse(amountString, NumberStyles.Any, CultureInfo.InvariantCulture, out size))
            {
                logger.Log(LogLevel.Error, "Experienced unparsable size: {0} in NewOrder message: {1}",
                                 amountString, messageString);
                return false;
            }

            //MinQty and LotSize not present
            if (messageString.Length - position == 0)
            {
                logger.Log(LogLevel.Warn, "Minqty and LotSize not present in NewOrder message: {0}", messageString);
                sequencedItchMessage = new NewOrderItchMessage(messageString, side, symbol, counterpartyOrderId, price, size, null, null);
                return true;
            }

            string minqtyString = messageString.Substring(position, 16);
            position += 16;

            decimal minQty;
            if (!decimal.TryParse(minqtyString, NumberStyles.Any, CultureInfo.InvariantCulture, out minQty))
            {
                logger.Log(LogLevel.Error, "Experienced unparsable MinQty: {0} in NewOrder message: {1}",
                                 minqtyString, messageString);
                return false;
            }

            string lotsizeString = messageString.Substring(position, 16);
            position += 16;

            decimal lotSize;
            if (!decimal.TryParse(lotsizeString, NumberStyles.Any, CultureInfo.InvariantCulture, out lotSize))
            {
                logger.Log(LogLevel.Error, "Experienced unparsable LotSize: {0} in NewOrder message: {1}",
                                 lotsizeString, messageString);
                return false;
            }

            sequencedItchMessage = new NewOrderItchMessage(messageString, side, symbol, counterpartyOrderId, price, size, minQty, lotSize);
            return true;
        }

    }
}

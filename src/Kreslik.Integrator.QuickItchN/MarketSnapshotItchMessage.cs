﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.QuickItchN
{
    public class MarketSnapshotItchMessage : SequencedDataItchMessage
    {
        public MarketSnapshotItchMessage(string rawMessage, List<ItchOrder> itchOrders)
            : base(rawMessage, ItchSequencedDataMessageSubtype.MarketSnapshot)
        {
            this.ItchOrders = itchOrders.Where(o => o != null).ToList().AsReadOnly();
        }

        public IReadOnlyList<ItchOrder> ItchOrders { get; private set; }

        public override string ToString()
        {
            return string.Format("MarketSnapshot, Orders: {1}{0}{1} {2}", string.Join(Environment.NewLine, ItchOrders),
                                 ItchOrders.Count > 0 ? Environment.NewLine : string.Empty, base.ToString());
        }

        public static bool TryParseMarketSnapshotMessage(string messageString, ref int position, ILogger logger, out SequencedDataItchMessage sequencedItchMessage)
        {
            sequencedItchMessage = null;

            if (messageString.Length - position < 7)
            {
                logger.Log(LogLevel.Error, "MarketSnapshot message size [{0}] is too low (Expected at least 7 in data packet part, was {1}): {2}", messageString.Length, messageString.Length - position, messageString);
                return false;
            }

            string lengthOfMessageString = messageString.Substring(position, 6);
            position += 6;
            int lengthOfMessage;
            if (!int.TryParse(lengthOfMessageString, out lengthOfMessage))
            {
                logger.Log(LogLevel.Error, "Experienced unparsable LengthOfMessage: {0} in MarketSnapshot message: {1}",
                                 lengthOfMessageString, messageString);
                return false;
            }

            //not counting the terminating LF
            if (messageString.Length - position - 1 != lengthOfMessage)
            {
                logger.Log(LogLevel.Error, "MarketSnapshot message body size [{0}] is unexpected (Expected {1}): {2}", messageString.Length - position - 1, lengthOfMessage, messageString);
                return false;
            }

            if (lengthOfMessage == 0 || messageString.Length >= ItchConstants.MAX_MESSAGE_SIZE)
            {
                sequencedItchMessage = new MarketSnapshotItchMessage(messageString, new List<ItchOrder>());
                return true;
            }

            string numberOfCurrencyPairsString = messageString.Substring(position, 4);
            position += 4;
            int numberOfCurrencyPairs;
            if (!int.TryParse(numberOfCurrencyPairsString, out numberOfCurrencyPairs))
            {
                logger.Log(LogLevel.Error, "Experienced unparsable NumberOfCurrencyPairs: {0} in MarketSnapshot message: {1}",
                                 numberOfCurrencyPairsString, messageString);
                return false;
            }

            List<ItchOrder> itchOrders = new List<ItchOrder>();

            for (int currencyPairIdx = 0; currencyPairIdx < numberOfCurrencyPairs; currencyPairIdx++)
            {
                if (messageString.Length - position < 11)
                {
                    logger.Log(LogLevel.Error, "Cannot read currency pair and number of bid prices on position: {0} in MarketSnapshot message: {1}",
                                 position, messageString);
                    return false;
                }

                string currencyPairString = messageString.Substring(position, 7);
                position += 7;
                Symbol symbol;
                if (!Symbol.TryParse(currencyPairString, out symbol))
                {
                    logger.Log(LogLevel.Warn,
                                     "Experienced (and ignoring) unknown currency pair: {0} in MarketSnapshot message: {1}",
                                     currencyPairString, messageString);
                }

                itchOrders.AddRange(ReadSidedItchOrdersFromMessage(messageString, ref position, logger, symbol, DealDirection.Buy));
                itchOrders.AddRange(ReadSidedItchOrdersFromMessage(messageString, ref position, logger, symbol, DealDirection.Sell));
            }

            sequencedItchMessage = new MarketSnapshotItchMessage(messageString, itchOrders);
            return true;
        }

        private static List<ItchOrder> ReadSidedItchOrdersFromMessage(string messageString, ref int position,
                                                                      ILogger logger, Symbol symbol, DealDirection side)
        {
            List<ItchOrder> itchOrders = new List<ItchOrder>();

            string numberOfPricesString = messageString.Substring(position, 4);
            position += 4;
            int numberOfPrices;
            if (!int.TryParse(numberOfPricesString, out numberOfPrices))
            {
                logger.Log(LogLevel.Error,
                                 "Experienced unparsable numberOfPrices: {0} in MarketSnapshot message: {1}",
                                 numberOfPricesString, messageString);
                return null;
            }

            for (int priceIdx = 0; priceIdx < numberOfPrices; priceIdx++)
            {
                if (messageString.Length - position < 14)
                {
                    logger.Log(LogLevel.Error,
                                     "Cannot read price and number of orders on position: {0} in MarketSnapshot message: {1}",
                                     position, messageString);
                    return null;
                }

                string priceString = messageString.Substring(position, 10);
                position += 10;
                decimal price;
                if (!decimal.TryParse(priceString, NumberStyles.Any, CultureInfo.InvariantCulture, out price))
                {
                    logger.Log(LogLevel.Error,
                                     "Experienced unparsable Price: {0} on position {1} in ModifyOrder message: {2}",
                                     priceString, position - 10, messageString);
                    return null;
                }

                string numberOfOrdersString = messageString.Substring(position, 4);
                position += 4;
                int numberOfOrders;
                if (!int.TryParse(numberOfOrdersString, out numberOfOrders))
                {
                    logger.Log(LogLevel.Error,
                                     "Experienced unparsable NumberOfOrders: {0} on position {1} in ModifyOrder message: {2}",
                                     numberOfOrders, position - 4, messageString);
                    return null;
                }

                for (int orderIdx = 0; orderIdx < numberOfOrders; orderIdx++)
                {
                    itchOrders.Add(ReadItchOrderFromMessage(messageString, ref position, logger, symbol, price, side, true));
                }
            }

            return itchOrders;
        }

        private static ItchOrder ReadItchOrderFromMessage(string messageString, ref int position, ILogger logger, Symbol symbol, decimal price, DealDirection side, bool readOptionalFields)
        {
            if ((!readOptionalFields && messageString.Length - position < 32) || (readOptionalFields && messageString.Length - position < 64))
            {
                logger.Log(LogLevel.Error, "Cannot read itch order fields on position: {0} in message: {1}",
                             position, messageString);
                return null;
            }

            string amountString = messageString.Substring(position, 16);
            position += 16;

            decimal size;
            if (!decimal.TryParse(amountString, NumberStyles.Any, CultureInfo.InvariantCulture, out size))
            {
                logger.Log(LogLevel.Error, "Experienced unparsable size: {0} in message: {1}",
                                 amountString, messageString);
                return null;
            }

            decimal? minQtyNullable = null;
            decimal? lotSizeNullable = null;
            //MinQty and LotSize
            if (readOptionalFields)
            {
                string minqtyString = messageString.Substring(position, 16);
                position += 16;

                decimal minQty;
                if (!decimal.TryParse(minqtyString, NumberStyles.Any, CultureInfo.InvariantCulture, out minQty))
                {
                    logger.Log(LogLevel.Error, "Experienced unparsable MinQty: {0} in message: {1}",
                                     minqtyString, messageString);
                    return null;
                }
                minQtyNullable = minQty;

                string lotsizeString = messageString.Substring(position, 16);
                position += 16;

                decimal lotSize;
                if (!decimal.TryParse(lotsizeString, NumberStyles.Any, CultureInfo.InvariantCulture, out lotSize))
                {
                    logger.Log(LogLevel.Error, "Experienced unparsable LotSize: {0} in message: {1}",
                                     lotsizeString, messageString);
                    return null;
                }
                lotSizeNullable = lotSize;
            }


            string counterpartyOrderId = messageString.Substring(position, 15);
            position += 15;

            if (symbol == Common.Symbol.NULL)
            {
                logger.Log(LogLevel.Error,
                                 "Experienced unknown symbol so parsing order [id: {0} size: {1}, price: {2}, side: {3}]",
                                 counterpartyOrderId, size, price, side);
                return null;
            }
            else
            {
                return new ItchOrder(side, symbol, counterpartyOrderId, price, size, minQtyNullable, lotSizeNullable);
            }
        }
    }
}

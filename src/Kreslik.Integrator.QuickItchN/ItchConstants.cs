﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kreslik.Integrator.QuickItchN
{
    public static class ItchConstants
    {
        public static readonly byte LF_BYTE = 0x0A;
        public static readonly int MAX_MESSAGE_SIZE = 100000000; //100M - aprox 0.1GB (which will need continuous memory)
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.QuickItchN
{
    public class ItchMessagesDuplexStream : IItchMessagesDuplexStream
    {
        private INetworkStreamWrapper _networkStreamWrapper;
        private ILogger _logger;
        private bool _closed = false;

        public ItchMessagesDuplexStream(string hostName, int port, ILogger logger)
        {
            this._networkStreamWrapper = new NetworkStreamWrapper(hostName, port, ItchConstants.MAX_MESSAGE_SIZE);
            this._logger = logger;
        }

        public void Initialize()
        {
            this._logger.Log(LogLevel.Info, "Initializing new ItchStream (wrapping {0})", this._networkStreamWrapper);
            this._networkStreamWrapper.Initialize();
        }

        public IEnumerable<IncomingItchMessage> IncomingMessages
        {
            get
            {
                while (!_closed)
                {
                    IncomingItchMessage itchMessage = null;
                    bool parsed = false;
                    DateTime receivedTime;

                    string messageString = _networkStreamWrapper.ReadNextToken(ItchConstants.LF_BYTE);
                    receivedTime = HighResolutionDateTime.UtcNow;
                    byte msgTypeByte = (byte)messageString[0];
                    IncomingItchMessageType messageType = IncomingItchMessage.GetMessageType(msgTypeByte);

                    //here we need factory pattern
                    switch (messageType)
                    {
                        case IncomingItchMessageType.LoginAccepted:
                            parsed = LoginAcceptedItchMessage.TryParseLoginAcceptedMessage(messageString, _logger, out itchMessage);
                            break;
                        case IncomingItchMessageType.LoginRejected:
                            parsed = LoginRejectedItchMessage.TryParseLoginRejectedMessage(messageString, _logger, out itchMessage);
                            break;
                        case IncomingItchMessageType.SequencedData:
                            parsed = SequencedDataItchMessage.TryParseSequncedDataMessage(messageString, _logger, out itchMessage);
                            break;
                        case IncomingItchMessageType.HeartBeat:
                            parsed = HeartBeatItchMessage.TryParseHeartBeatMessage(messageString, _logger, out itchMessage);
                            break;
                        case IncomingItchMessageType.ErrorNotification:
                            parsed = ErrorNotificationItchMessage.TryParseErrorMessage(messageString, _logger, out itchMessage);
                            break;
                        case IncomingItchMessageType.InstrumentDirectory:
                            parsed = InstrumentDirectoryItchMessage.TryParseInstrumentDirectoryMessage(messageString, _logger, out itchMessage);
                            break;
                        case IncomingItchMessageType.Unknown:
                        default:
                            this._logger.Log(LogLevel.Fatal, "Unexpected type of message: {0}, message: {1}",
                                                messageType, messageString);
                            break;
                    }

                    if (parsed && itchMessage != null)
                    {
                        itchMessage.IntegratorReceivedTimeUtc = receivedTime;
                        yield return itchMessage;
                    }
                }

                yield break;
            }
        }

        public void SendMessage(OutgoingItchMessage message)
        {
            if (_closed)
            {
                this._logger.Log(LogLevel.Error, "Attempt to send Itch message through closed stream: {0}", message);
            }
            else
            {
                this._logger.Log(LogLevel.Info, "Sending Itch message: {0}", message);
                this._networkStreamWrapper.SendString(message.RawMessage);
            }
        }

        public void Close()
        {
            this._logger.Log(LogLevel.Info, "Closing network stream {0}", this);
            Volatile.Write(ref _closed, true);
            this._networkStreamWrapper.Stop();
        }
    }

}

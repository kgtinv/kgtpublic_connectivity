﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.QuickItchN
{
    public class StreamWrapper : IStreamWrapper
    {
        private Stream _stream;
        private const int _SHARED_BUFFER_LEN = 1024;
        private readonly byte[] _sharedBuffer = new byte[_SHARED_BUFFER_LEN];
        private int _sharedBufferStartPos = 0;
        private int _sharedBufferLen = 0;
        private readonly int _maxSingleMessageSize;

        public StreamWrapper(Stream stream, int maxSingleMessageSize)
        {
            this._stream = stream;
            this._maxSingleMessageSize = maxSingleMessageSize;
        }

        //TODO: callers need to handle IOException and SocketException
        public string ReadNextToken(byte tokenSeparator)
        {
            StringBuilder sb = new StringBuilder();

            bool nextTokenFound = false;
            do
            {
                if (_sharedBufferLen > 0)
                {
                    int lfIndex = Array.IndexOf(_sharedBuffer, tokenSeparator, _sharedBufferStartPos, _sharedBufferLen);
                    if (lfIndex >= _sharedBufferStartPos && lfIndex < _sharedBufferStartPos + _sharedBufferLen)
                    {
                        sb.Append(System.Text.Encoding.ASCII.GetString(_sharedBuffer, _sharedBufferStartPos,
                                                                       lfIndex - _sharedBufferStartPos + 1));
                        if (lfIndex < _sharedBufferStartPos + _sharedBufferLen)
                        {
                            _sharedBufferLen -= (lfIndex + 1 - _sharedBufferStartPos);
                            _sharedBufferStartPos = lfIndex + 1;
                        }
                        else
                        {
                            _sharedBufferLen = 0;
                            _sharedBufferStartPos = 0;
                        }
                        nextTokenFound = true;
                    }
                    else
                    {
                        sb.Append(System.Text.Encoding.ASCII.GetString(_sharedBuffer, _sharedBufferStartPos,
                                                                       _sharedBufferLen));
                        _sharedBufferLen = 0;
                        _sharedBufferStartPos = 0;
                    }
                }

                //If we exceeded allowed msg size - let's end even if we haven't found next token
                if (sb.Length >= this._maxSingleMessageSize)
                {
                    nextTokenFound = true;
                }

                if (!nextTokenFound)
                {
                    //This is the critical call which can take the most time, however it
                    // cannot be optimized as waiting network read means that data are not available
                    // and that sender is blocking
                    _sharedBufferLen = _stream.Read(_sharedBuffer, 0, _SHARED_BUFFER_LEN);
                    _sharedBufferStartPos = 0;
                    if (_sharedBufferLen == 0)
                        throw new SocketException(System.Convert.ToInt32(SocketError.ConnectionReset));
                }

            } while (!nextTokenFound);

            return sb.ToString();
        }
    }
}

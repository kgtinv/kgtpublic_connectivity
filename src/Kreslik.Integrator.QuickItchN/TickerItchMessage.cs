﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.QuickItchN
{
    public class TickerItchMessage : SequencedDataItchMessage
    {
        public TickerItchMessage(string rawMessage, TradeSide aggressorSide, Symbol symbol, decimal price, DateTime counterpartySuppliedTransactionTime)
            : base(rawMessage, ItchSequencedDataMessageSubtype.Ticker)
        {
            this.AggressorSide = aggressorSide;
            this.Symbol = symbol;
            this.Price = price;
            this.CounterpartySuppliedTransactionTime = counterpartySuppliedTransactionTime;
        }

        public TradeSide AggressorSide { get; private set; }
        public Symbol Symbol { get; private set; }
        public decimal Price { get; private set; }
        public DateTime CounterpartySuppliedTransactionTime { get; private set; }

        public override string ToString()
        {
            return string.Format("Ticker - AggresorSide: {0}, Symbol: {1}, Price: {2}, TransactionTime: {3}. {4}",
                                 AggressorSide, Symbol, Price, CounterpartySuppliedTransactionTime, base.ToString());
        }

        public static bool TryParseTickerMessage(string messageString, ref int position, ILogger logger, out SequencedDataItchMessage sequencedItchMessage)
        {
            sequencedItchMessage = null;

            if (messageString.Length - position != 33)
            {
                logger.Log(LogLevel.Error, "Ticker message size [{0}] is unexpected (Expected 33 in data packet part, was {1}): {2}", messageString.Length, messageString.Length - position, messageString);
                return false;
            }

            TradeSide side;
            switch (messageString[position])
            {
                case 'B':
                    side = TradeSide.BuyPaid;
                    break;
                case 'S':
                    side = TradeSide.SellGiven;
                    break;
                default:
                    logger.Log(LogLevel.Error,
                                     "Experienced unexpected side indicator character: {0} in Ticker message: {1}",
                                     messageString[position], messageString);
                    return false;
            }
            position++;

            Symbol symbol;
            if (!Symbol.TryParse(messageString.Substring(position, 7), out symbol))
            {
                logger.Log(LogLevel.Error, "Experienced unknown currency pair: {0} in Ticker message: {1}",
                                 messageString.Substring(position, 7), messageString);
                return false;
            }
            position += 7;

            string priceString = messageString.Substring(position, 10);
            position += 10;

            decimal price;
            if (!decimal.TryParse(priceString, NumberStyles.Any, CultureInfo.InvariantCulture, out price))
            {
                logger.Log(LogLevel.Error, "Experienced unparsable price: {0} in Ticker message: {1}",
                                 priceString, messageString);
                return false;
            }

            string timeString = messageString.Substring(position, 14);
            position += 14;

            DateTime counterpartyTransactionTime;
            if (!DateTime.TryParseExact(timeString, "yyyyMMddHHmmss", CultureInfo.InvariantCulture, DateTimeStyles.None,
                                        out counterpartyTransactionTime))
            {
                logger.Log(LogLevel.Error, "Experienced unparsable time: {0} in Ticker message: {1}",
                                 timeString, messageString);
                return false;
            }
            counterpartyTransactionTime = TradingHoursHelper.Instance.ConvertFromEtToUtc(counterpartyTransactionTime);

            sequencedItchMessage = new TickerItchMessage(messageString, side, symbol, price, counterpartyTransactionTime);
            return true;
        }
    }
}

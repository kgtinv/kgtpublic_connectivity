﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.QuickItchN
{
    public class LoginRejectedItchMessage : IncomingItchMessage
    {
        public LoginRejectedItchMessage(string rawMessage, string reason)
            : base(rawMessage, IncomingItchMessageType.LoginRejected)
        {
            this.Reason = reason;
        }

        public override bool IsAdminMessage
        {
            get { return true; }
        }

        public string Reason { get; private set; }

        public override string ToString()
        {
            return string.Format("Login rejected, reason: {0}, {1}", Reason, base.ToString());
        }

        public static bool TryParseLoginRejectedMessage(string messageString, ILogger logger, out IncomingItchMessage itchMessage)
        {
            itchMessage = null;
            int position = 1;

            if (messageString.Length != 22)
            {
                logger.Log(LogLevel.Error, "LogonRejected message size [{0}] is unexpected (Expected 22): {1}", messageString.Length, messageString);
                return false;
            }

            string reason = messageString.Substring(position, 20);
            position += 20;

            if (messageString[position] != ItchConstants.LF_BYTE)
            {
                logger.Log(LogLevel.Error, "Unexpected termination char [{0}] in LogonRejected message: {1}", messageString[position], messageString);
                return false;
            }

            itchMessage = new LoginRejectedItchMessage(messageString, reason);
            return true;
        }
    }
}

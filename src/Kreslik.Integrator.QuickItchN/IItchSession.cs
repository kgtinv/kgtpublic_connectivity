﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kreslik.Integrator.Common;

namespace Kreslik.Integrator.QuickItchN
{
    public enum ItchSessionState
    {
        Unconfigured,
        Created,
        Starting,
        Running,
        Stopping,
        Stopped
    }

    public interface IItchSession
    {
        void Configure(string hostName, int port, string login, string password, TimeSpan heartBeatInterval,
                       ILogger logger);
        void Configure(ILogger logger, HotspotCounterparty counterparty);
        bool Start();
        bool Stop();
        void Restart();
        event Action LoggedIn;
        event Action LoggedOut;
        ItchSessionState ItchSessionState { get; }
        event Action<ItchSessionState> StateChanging;
        bool Inactivated { get; }
        event Action<IncomingItchMessage> NewIncomingItchMessage;
        void SendMessage(OutgoingItchMessage message);
    }
}

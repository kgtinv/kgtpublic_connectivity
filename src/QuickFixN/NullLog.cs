﻿
namespace QuickFix
{
    /// <summary>
    /// Log implementation that does not do anything
    /// </summary>
    public class NullLog : ILog
    {
        #region Log Members

        public void Clear()
        { }

        public void OnIncoming(string msg)
        { }

        public void OnOutgoing(string msg)
        { }

        public void OnEvent(string s)
        { }

        public void OnFatalEvent(string s)
        { }

        public void OnExpectedEvent(string s)
        { }

        #endregion
    }
}
